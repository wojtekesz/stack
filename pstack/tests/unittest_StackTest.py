# coding=utf-8
# importowanie modułów zależnych
import unittest

from stack.stack import Stack


class unittest_StackTest(unittest.TestCase):
    stack = None

    # metoda ta pozwala ustawić pewien stan lub poczynić przygotowania przed każdym testem
    # metody setUp i tearDown są uruchamiane automatycznie przez środowisko uruchomieniowe testó
    def setUp(self):
        self.stack = Stack()

    # metoda czyszcząca stan stosu po każdym teście
    def tearDown(self):
        self.stack = None

    # wyznacznikiem testu jednostkowego jest prefix test_
    def test_push(self):
        # given
        item = 'item1'

        # when
        self.stack.push(item)

        # then
        # elementy assert* pozwalają tworzyć asercje/zapewnienia, po których spełnieniu możemy stwierdzić że test zakończył się bez błędu
        self.assertEqual(1, self.stack.size())
        self.assertEqual(['item1'], self.stack.get_collection())

    def test_pop(self):
        # given
        item = 'item1'
        self.stack.push(item)

        # when
        result = self.stack.pop()

        # then
        self.assertEqual('item1', result)
        self.assertEqual(0, self.stack.size())

    def test_get_stack(self):
        # given
        item = 'item1'
        self.stack.push(item)

        # when
        result = self.stack.get_collection()

        # then
        self.assertEqual(['item1'], result)
