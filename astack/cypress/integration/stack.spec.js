describe('Testing the hero list page', function() {

  it('should get stack page', function() {
    cy.visit('http://localhost:4200');
    cy.contains("astack");
  })
  
it('should push elements', function() {
  cy.visit('http://localhost:4200');
  cy.get("input").type("data1");
  cy.get(".push-button").click();
  cy.get("input").clear().type("data2");
  cy.get(".push-button").click();

  cy.get(".stack-content")
    .find("div")
    .contains("data2")
    .next("div")
    .contains("data1");
})

it('should pop elements', function() {
  cy.visit('http://localhost:4200');
  cy.get("input").type("data1");
  cy.get(".push-button").click();
  cy.get("input").clear().type("data2");
  cy.get(".push-button").click();

  cy.get(".pop-button").click().click();
  cy.get(".size").contains("Stack size: 0");
  cy.get(".stack-content").find("div").should("not.exist");
})

});
