import { Component } from '@angular/core';
import { Stack } from './stack/stack.service';

@Component({
    selector: 'app-root',
    template: `
        <div class="header">
        <h1>
            Welcome to {{ title }}!
        </h1>
        </div>
        <div class="container">
            <input #textfield>
            <button class="push-button" (click)="push(textfield.value)">push</button>
            <button class="pop-button" (click)="onPop()">pop</button>
        </div>
        <div class="results">
            <div class="stack">Stack content: {{elementsAsJson}}</div>
            <div class="size">Stack size: {{size}}</div>
            <div class="last-popped">Last popped: {{last}}</div>
        </div>
        <div class="stack-content">
            <div *ngFor="let element of reversedElements"
                 class="stack-element">
            {{element}}
            </div>
        </div>
        `,
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'astack';
    last = '';

    constructor(private stack: Stack) { }

    push(element: any): void {
        this.stack.push(element);
    }

    onPop(): void {
        this.last = this.pop();
    }

    pop(): any {
        if (this.stack.size() > 0) {
            return this.stack.pop();
        } else {
            return 'no element';
        }
    }

    get reversedElements(): any[] {
        const elements = [...this.stack.elements()];
        elements.reverse();
        return elements;
    }

    get elementsAsJson(): string {
        return JSON.stringify(this.stack.elements());
    }

    get size(): number {
        return this.stack.size();
    }
}
